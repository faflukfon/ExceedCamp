const url = "http://ecourse.cpe.ku.ac.th:1515/api/";
let preflix = "palmwith-";
$(function () {
  let li = 0;
  let hud = 0;
  let temp = 0;

  $("#air-button").on("click", function () {
    $.ajax({
      type: "POST",
      url: `${url}${preflix}air_status/set?value=1`,
      dataType: "json",
      success: function (response) { }
    });
  });

  $("#led-button").on("click", function () {
    $.ajax({
      type: "POST",
      url: `${url}${preflix}led_in_status/set?value=1`,
      dataType: "json",
      success: function (response) { }
    });
  });

  $("#ring-button").on("click", function () {
    $.ajax({
      type: "POST",
      url: `${url}${preflix}buzzer_status/set?value=1`,
      dataType: "json",
      success: function (response) { }
    });
  });

  $("#door-button").on("click", function () {
    $.ajax({
      type: "POST",
      url: `${url}${preflix}door_status/set?value=1`,
      dataType: "json",
      success: function (response) { }
    });
  });

  setInterval(function () {
    $.ajax({
      type: "GET",
      url: `${url}${preflix}light_intensity_status/view`,
      dataType: "json",
      success: function (response) {
        li = response;
        $("#li-display").html(`<h1> ${li} Lux</h1>`)
        console.log(response);
      },
      fail: function (response) {
        console.log(response);
      }
    });
    $.ajax({
      type: "GET",
      url: `${url}${preflix}temperature_status/view`,
      dataType: "json",
      success: function (response) {
        temp = response;
        $("#temp-display").html(`<h1>${temp} C</h1>`)
        console.log(response);
      },
      fail: function (response) {
        console.log(response);
      }
    });

    $.ajax({
      type: "GET",
      url: `${url}${preflix}humidity_status/view`,
      dataType: "json",
      success: function (response) {
        hud = response;
        $("#hud-display").html(`<h1>${hud} %</h2>`)
        console.log(response);
      },
      fail: function (response) {
        console.log(response);
      }
    });

  }, 2000);

  window.onload = function () {

    //Better to construct options first and then pass it as a parameter
    let options = {
      title: {
        text: "Status"
      },
      data: [
        {
          // Change type to "doughnut", "line", "splineArea", etc.
          type: "column",
          dataPoints: [
            { label: "Ligth intensity", y: li },
            { label: "Temperature", y: temp },
            { label: "Humidity", y: hud },
          ]
        }
      ]
    };

    $("#chartContainer").CanvasJSChart(options);
  }
});
